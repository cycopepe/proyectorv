#ifndef __VRPN_CONTROLLER__H__
#define __VRPN_CONTROLLER__H__

#include <vector>

#include "BaseController.h"
#include "../devices/VRPN_Device.h"

class VRPN_Controller
  : public BaseController
{
public:
  typedef VRPN_Controller Self;
  typedef BaseController  Superclass;

  typedef std::vector< VRPN_Device* > TDevices;

public:
  VRPN_Controller(
    Ogre::RenderWindow* win,
    Ogre::Camera* cam,
    BaseView* view,
    bool bufferedKeys = false,
    bool bufferedMouse = false
    );
  virtual ~VRPN_Controller( );

  virtual bool frameRenderingQueued( const Ogre::FrameEvent& evt );

protected:
  TDevices m_Devices;
};

#endif // __VRPN_CONTROLLER__H__

// eof - $RCSfile$
