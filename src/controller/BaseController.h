#ifndef __BASECONTROLLER__H__
#define __BASECONTROLLER__H__

#include <Ogre.h>
#include <OgreStringConverter.h>
#include <OgreException.h>

//#include <SdkTrays.h>

//Use this define to signify OIS will be used as a DLL
//(so that dll import/export macros are in effect)
#define OIS_DYNAMIC_LIB
#include <OIS/OIS.h>



class BaseView;

class BaseController
  : public Ogre::FrameListener,
    public Ogre::WindowEventListener  
{
public:
  BaseController(
    Ogre::RenderWindow* win,
    Ogre::Camera* cam,
    BaseView* view,
    bool bufferedKeys = false,
    bool bufferedMouse = false
    );
  virtual ~BaseController( );

  virtual void windowResized( Ogre::RenderWindow* rw );
  virtual void windowClosed( Ogre::RenderWindow* rw );
  virtual bool processUnbufferedKeyInput( const Ogre::FrameEvent& evt );
  virtual bool processUnbufferedMouseInput( const Ogre::FrameEvent& evt );
  virtual void moveCamera( );
  virtual bool frameRenderingQueued( const Ogre::FrameEvent& evt );
  virtual bool frameEnded( const Ogre::FrameEvent& evt );

protected:
  Ogre::Camera*       m_Camera;
  Ogre::Vector3       m_TranslateVector;
  Ogre::Real          m_CurrentSpeed;
  Ogre::RenderWindow* m_Window;

  float        m_MoveScale;
  Ogre::Degree m_RotScale;

  Ogre::Radian m_RotX;
  Ogre::Radian m_RotY;

  Ogre::Real   m_MoveSpeed;
  Ogre::Degree m_RotateSpeed;

  // OIS Input devices
  OIS::InputManager* m_InputManager;
  OIS::Mouse*        m_Mouse;
  OIS::Keyboard*     m_Keyboard;

  // M-C
  BaseView* m_View;
  
  //OgreBites
  //OgreBites::SdkTrayManager* mTrayMgr;
};

#endif // __BASECONTROLLER__H__

// eof - $RCSfile$
