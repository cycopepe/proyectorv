#include "VRPN_Controller.h"
#include "../devices/VRPN_WiiMote.h"
#include <algorithm>

// -------------------------------------------------------------------------
VRPN_Controller::
VRPN_Controller(
  Ogre::RenderWindow* win,
  Ogre::Camera* cam,
  BaseView* view,
  bool bufferedKeys,
  bool bufferedMouse
  )
  : Superclass( win, cam, view, bufferedKeys, bufferedMouse )
{
  VRPN_WiiMote* w0 = new VRPN_WiiMote( );
  if( !( w0->connect( "WiiMote0@localhost" ) ) )
  {
    Ogre::LogManager::getSingletonPtr( )->
      logMessage( "!!! Error connecting to WiiMote0@localhost !!!" );
    delete w0;
    w0 = NULL;

  } // fi
  this->m_Devices.push_back( w0 );
}

// -------------------------------------------------------------------------
VRPN_Controller::
~VRPN_Controller( )
{
  TDevices::iterator dIt = this->m_Devices.begin( );
  for( ; dIt != this->m_Devices.end( ); dIt++ )
    if( *dIt != NULL )
      delete *dIt;
}

// -------------------------------------------------------------------------
bool VRPN_Controller::
frameRenderingQueued( const Ogre::FrameEvent& evt )
{
  if( this->m_Devices[ 0 ] != NULL )
  {
    VRPN_WiiMote* wii = dynamic_cast< VRPN_WiiMote* >( this->m_Devices[ 0 ] );
    VRPN_WiiMote_State wii_state = wii->capture( );

Ogre::Real moveScale = this->m_MoveScale;

    if(wii_state.isButton1Pushed()){
        Ogre::LogManager::getSingletonPtr( )->
      logMessage( "+++  button 1 pushed" );
  this->m_TranslateVector.x = -moveScale; // Move camera left
this->moveCamera( );
    }
    if(wii_state.isButton2Pushed()){
        Ogre::LogManager::getSingletonPtr( )->
      logMessage( "+++  button 2 pushed" );
  this->m_TranslateVector.x = moveScale; // Move camera left
this->moveCamera( );
    }

 if(wii_state.isButtonUpPushed()){
        Ogre::LogManager::getSingletonPtr( )->
      logMessage( "+++  button 2 pushed" );
  this->m_TranslateVector.z = moveScale; // Move camera left
this->moveCamera( );
    }

 if(wii_state.isButtonDownPushed()){
        Ogre::LogManager::getSingletonPtr( )->
      logMessage( "+++  button 2 pushed" );
  this->m_TranslateVector.z = -moveScale; // Move camera left
this->moveCamera( );
    }

 if(wii_state.isButtonLeftPushed()){
        Ogre::LogManager::getSingletonPtr( )->
      logMessage( "+++  button 2 pushed" );
  this->m_TranslateVector.x =- moveScale; // Move camera left
this->moveCamera( );
    }

 if(wii_state.isButtonRightPushed()){
        Ogre::LogManager::getSingletonPtr( )->
      logMessage( "+++  button 2 pushed" );
  this->m_TranslateVector.x = moveScale; // Move camera left
this->moveCamera( );
    }
  } // fi
  return( this->Superclass::frameRenderingQueued( evt ) );
}

// eof - $RCSfile$
