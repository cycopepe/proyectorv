#include "BaseController.h"
#include "../view/BaseView.h"

// -------------------------------------------------------------------------
BaseController::
BaseController(
  Ogre::RenderWindow* win,
  Ogre::Camera* cam,
  BaseView* view,
  bool bufferedKeys,
  bool bufferedMouse
  ) : m_Camera( cam ),
      m_View( view ),
      m_TranslateVector( Ogre::Vector3::ZERO ),
      m_CurrentSpeed( 0 ),
      m_Window( win ),
      m_MoveScale( 0.0f ),
      m_RotScale( 0.0f ),
      m_MoveSpeed( 100 ),
      m_RotateSpeed( 36 ),
      m_InputManager( 0 ),
      m_Mouse( 0 ),
      m_Keyboard( 0 )
{
  Ogre::LogManager::getSingletonPtr( )->
    logMessage( "*** Initializing OIS ***" );
  OIS::ParamList pl;
  size_t windowHnd = 0;
  std::ostringstream windowHndStr;

  win->getCustomAttribute( "WINDOW", &windowHnd );
  windowHndStr << windowHnd;
  pl.insert( std::make_pair( std::string( "WINDOW" ), windowHndStr.str( ) ) );

  this->m_InputManager = OIS::InputManager::createInputSystem( pl );

  this->m_Keyboard =
    static_cast< OIS::Keyboard* >(
      this->m_InputManager->createInputObject(
        OIS::OISKeyboard, bufferedKeys
        )
      );
  this->m_Mouse =
    static_cast< OIS::Mouse* >(
      this->m_InputManager->createInputObject(
        OIS::OISMouse, bufferedMouse
        )
      );

  // Set initial mouse clipping size
  this->windowResized( this->m_Window );

  // Register as a Window listener
  Ogre::WindowEventUtilities::
    addWindowEventListener( this->m_Window, this );
}

// -------------------------------------------------------------------------
BaseController::
~BaseController( )
{
  // Remove ourself as a Window listener
  Ogre::WindowEventUtilities::
    removeWindowEventListener( this->m_Window, this );
  windowClosed( this->m_Window );
}

// -------------------------------------------------------------------------
void BaseController::
windowResized( Ogre::RenderWindow* rw )
{
  unsigned int width, height, depth;
  int left, top;
  rw->getMetrics( width, height, depth, left, top );

  const OIS::MouseState &ms = this->m_Mouse->getMouseState( );
  ms.width = width;
  ms.height = height;
}

// -------------------------------------------------------------------------
void BaseController::
windowClosed( Ogre::RenderWindow* rw )
{
  if( rw == this->m_Window )
  {
    if( this->m_InputManager )
    {
      this->m_InputManager->destroyInputObject( this->m_Mouse );
      this->m_InputManager->destroyInputObject( this->m_Keyboard );

      OIS::InputManager::destroyInputSystem( this->m_InputManager );
      this->m_InputManager = 0;

    } // fi

  } // fi
}

// -------------------------------------------------------------------------
bool BaseController::
processUnbufferedKeyInput( const Ogre::FrameEvent& evt )
{
  Ogre::Real moveScale = this->m_MoveScale;
  if( this->m_Keyboard->isKeyDown( OIS::KC_LSHIFT ) )
    moveScale *= 10;

  if( this->m_Keyboard->isKeyDown( OIS::KC_A ) )
    this->m_TranslateVector.x = -moveScale; // Move camera left

  if( this->m_Keyboard->isKeyDown( OIS::KC_D ) )
    this->m_TranslateVector.x = moveScale;  // Move camera right

  if(
    this->m_Keyboard->isKeyDown( OIS::KC_UP ) ||
    this->m_Keyboard->isKeyDown( OIS::KC_W )
    )
    this->m_TranslateVector.z = -moveScale; // Move camera forward

  if(
    this->m_Keyboard->isKeyDown( OIS::KC_DOWN ) ||
    this->m_Keyboard->isKeyDown( OIS::KC_S )
    )
    this->m_TranslateVector.z = moveScale;  // Move camera backward

  if( this->m_Keyboard->isKeyDown( OIS::KC_PGUP ) )
    this->m_TranslateVector.y = moveScale;  // Move camera up

  if( this->m_Keyboard->isKeyDown( OIS::KC_PGDOWN ) )
    this->m_TranslateVector.y = -moveScale; // Move camera down

  if( this->m_Keyboard->isKeyDown( OIS::KC_RIGHT ) )
    this->m_Camera->yaw( -this->m_RotScale );

  if( this->m_Keyboard->isKeyDown( OIS::KC_LEFT ) )
    this->m_Camera->yaw( this->m_RotScale );

  if(
    this->m_Keyboard->isKeyDown( OIS::KC_ESCAPE ) ||
    this->m_Keyboard->isKeyDown( OIS::KC_Q )
    )
    return( false );

  // Return true to continue rendering
  return( true );
}

// -------------------------------------------------------------------------
bool BaseController::
processUnbufferedMouseInput( const Ogre::FrameEvent& evt )
{
  // Rotation factors, may not be used if the second mouse button is pressed
  // 2nd mouse button - slide, otherwise rotate
  const OIS::MouseState &ms = this->m_Mouse->getMouseState( );
  if( ms.buttonDown( OIS::MB_Right ) )
  {
    this->m_TranslateVector.x += ms.X.rel * Ogre::Real( 0.13 );
    this->m_TranslateVector.y -= ms.Y.rel * Ogre::Real( 0.13 );
  }
  else
  {
    this->m_RotX = Ogre::Degree( -ms.X.rel * Ogre::Real( 0.13 ) );
    this->m_RotY = Ogre::Degree( -ms.Y.rel * Ogre::Real( 0.13 ) );

  } // fi
  return( true );
}

// -------------------------------------------------------------------------
void BaseController::
moveCamera( )
{
  // Make all the changes to the camera
  this->m_Camera->yaw( this->m_RotX );
  this->m_Camera->pitch( this->m_RotY );
  this->m_Camera->moveRelative( this->m_TranslateVector );
}

// -------------------------------------------------------------------------
bool BaseController::
frameRenderingQueued( const Ogre::FrameEvent& evt )
{
  if( this->m_Window->isClosed( ) )
    return( false );

  // TODO: Check model
  Ogre::Real timeoffset = evt.timeSinceLastFrame;

  // Need to capture/update each device
  this->m_Keyboard->capture( );
  this->m_Mouse->capture( );

  Ogre::Vector3 lastMotion = this->m_TranslateVector;

  // Check if one of the devices is not buffered
  if( !this->m_Mouse->buffered( ) || !this->m_Keyboard->buffered( ) )
  {
    // Move about 100 units per second
    this->m_MoveScale = this->m_MoveSpeed * timeoffset;

    // Take about 10 seconds for full rotation
    this->m_RotScale = this->m_RotateSpeed * timeoffset;

    this->m_RotX = 0;
    this->m_RotY = 0;
    this->m_TranslateVector = Ogre::Vector3::ZERO;

  } // fi

  if( !this->m_Keyboard->buffered( ) )
    if( this->processUnbufferedKeyInput( evt ) == false )
      return( false );

  if( !this->m_Mouse->buffered( ) )
    if( this->processUnbufferedMouseInput( evt ) == false )
      return( false );

  // ramp up / ramp down speed
  if ( this->m_TranslateVector == Ogre::Vector3::ZERO )
  {
    // decay ( one third speed )
    this->m_CurrentSpeed -= timeoffset * Ogre::Real( 0.3 );
    this->m_TranslateVector = lastMotion;
  }
  else
  {
    // ramp up
    this->m_CurrentSpeed += timeoffset;

  }
  // Limit motion speed
  if ( this->m_CurrentSpeed > 1.0 )
    this->m_CurrentSpeed = 1.0;
  if ( this->m_CurrentSpeed < 0.0 )
    this->m_CurrentSpeed = 0.0;

  this->m_TranslateVector *= this->m_CurrentSpeed;

  if( !this->m_Mouse->buffered( ) || !this->m_Keyboard->buffered( ) )
    this->moveCamera( );

  // Update simulations/animations
  this->m_View->updateSimulation( timeoffset );

  return( true );
}

// -------------------------------------------------------------------------
bool BaseController::
frameEnded( const Ogre::FrameEvent& evt )
{
  return( true );
}

// eof - $RCSfile$
