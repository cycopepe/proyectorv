// -------------------------------------------------------------------------
// OGRE objects
// -------------------------------------------------------------------------

#include "view/BaseView.h"
typedef BaseView TApplication;

// -------------------------------------------------------------------------
// Main function
// -------------------------------------------------------------------------

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
  INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
#else // OGRE_PLATFORM == OGRE_PLATFORM_WIN32
  int main( int argc, char* argv[ ] )
#endif // OGRE_PLATFORM == OGRE_PLATFORM_WIN32
  {
    TApplication app;
    try
    {
      app.run( );
    }
    catch( Ogre::Exception& err )
    {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
      MessageBox(
        NULL,
        err.getFullDescription( ).c_str( ),
        "An exception has occured!",
        MB_OK | MB_ICONERROR | MB_TASKMODAL
        );
#else // OGRE_PLATFORM == OGRE_PLATFORM_WIN32
      std::cerr
        << "An exception has occured: "
        << err.getFullDescription( )
        << std::endl;
#endif // OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    } // yrt
    return( 0 );
  }
#ifdef __cplusplus
} // nretxe
#endif // __cplusplus

// eof - main.cxx
