#include "BaseView.h"

// -------------------------------------------------------------------------

BaseView::
BaseView()
: m_ResourcePath(""),
m_ConfigPath(""),
m_Root(NULL),
m_SceneMgr(NULL),
m_Window(NULL),
m_Camera(NULL),
m_Controller(NULL) {
}

// -------------------------------------------------------------------------

BaseView::
~BaseView() {
    if (this->m_Controller)
        delete this->m_Controller;
    if (this->m_Root)
        OGRE_DELETE this->m_Root;
}

// -------------------------------------------------------------------------

void BaseView::
run() {
    if (!this->setup())
        return;
    this->m_Root->startRendering();
    this->_0_destroyScene();
}

// -------------------------------------------------------------------------

void BaseView::
updateSimulation(const Ogre::Real& timeoffset) {
    this->m_AnimationState->addTime(timeoffset);
}

// -------------------------------------------------------------------------

bool BaseView::
setup() {
    Ogre::String pluginsPath = this->m_ResourcePath + "plugins.cfg";

    // Create root entity
    this->m_Root =
            OGRE_NEW Ogre::Root(
            pluginsPath,
            this->m_ConfigPath + "ogre.cfg",
            this->m_ResourcePath + "Ogre.log"
            );

    // Initialization sequence
    this->_1_setupResources();
    if (!this->_2_configure())
        return ( false);
    this->_3_chooseSceneManager();
    this->_4_createCamera();
    this->_5_createViewports();
    this->_6_createResourceListener();
    this->_7_loadResources();
    this->_8_createScene();
    this->_9_createFrameListener();
    this->_10_createAnimations();

    return ( true);
}

// -------------------------------------------------------------------------

void BaseView::
_0_destroyScene() {
}


// -------------------------------------------------------------------------

void BaseView::
_1_setupResources() {
    // Load resource paths from config file
    Ogre::ConfigFile cf;
    cf.load(this->m_ResourcePath + "resources.cfg");

    // Go through all sections & settings in the file
    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
    Ogre::String secName, typeName, archName;
    while (seci.hasMoreElements()) {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap* settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i) {
            typeName = i->first;
            archName = i->second;
            Ogre::ResourceGroupManager::getSingleton().
                    addResourceLocation(
                    archName, typeName, secName
                    );

        } // rof

    } // elihw
}

// -------------------------------------------------------------------------

bool BaseView::
_2_configure() {
    if (!this->m_Root->restoreConfig()) {
        this->m_Window = this->m_Root->initialise(true);
        return ( true);
    } else {
        if (this->m_Root->showConfigDialog()) {
            this->m_Window = this->m_Root->initialise(true, "MedicalRV");

            return true;
        }
    }
    return ( false);
}

// -------------------------------------------------------------------------

void BaseView::
_3_chooseSceneManager() {
    // Create the SceneManager, in this case a generic one
    this->m_SceneMgr = this->m_Root->createSceneManager(
            Ogre::ST_GENERIC,
            "BaseViewSceneMgr"
            );
}

// -------------------------------------------------------------------------

void BaseView::
_4_createCamera() {
    this->m_Camera = this->m_SceneMgr->createCamera("BaseViewCamera");
    this->m_Camera->setPosition(Ogre::Vector3(25, 25, 25));
    this->m_Camera->lookAt(Ogre::Vector3(0, 10, 0));
    this->m_Camera->setNearClipDistance(5);
}

// -------------------------------------------------------------------------

void BaseView::
_5_createViewports() {
    // Create one viewport, entire window
    Ogre::Viewport* vp = this->m_Window->addViewport(this->m_Camera);
    vp->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

    // Alter the camera aspect ratio to match the viewport
    this->m_Camera->setAspectRatio(
            Ogre::Real(vp->getActualWidth()) /
            Ogre::Real(vp->getActualHeight())
            );
}

// -------------------------------------------------------------------------

void BaseView::
_6_createResourceListener() {
    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
}

// -------------------------------------------------------------------------

void BaseView::
_7_loadResources() {
    Ogre::ResourceGroupManager::getSingleton().
            initialiseAllResourceGroups();
}

// -------------------------------------------------------------------------

void BaseView::
_8_createScene() {
    // Lights
    this->m_SceneMgr->setAmbientLight(Ogre::ColourValue(1, 1, 1));
    this->m_SceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

    Ogre::Light* light1 = this->m_SceneMgr->createLight("Light1");
    light1->setType(Ogre::Light::LT_POINT);
    light1->setPosition(Ogre::Vector3(50, 50, 50));
    light1->setDiffuseColour(1.0, 1.0, 1.0);
    light1->setSpecularColour(1.0, 1.0, 1.0);

    Ogre::Light* light2 = this->m_SceneMgr->createLight("Light2");
    light2->setType(Ogre::Light::LT_POINT);
    light2->setPosition(Ogre::Vector3(0, 50, -50));
    light2->setDiffuseColour(0.5, 0.5, 0.5);
    light2->setSpecularColour(0.5, 0.5, 0.5);

    // Create a plane
    Ogre::Plane plane(Ogre::Vector3::UNIT_Y, 0);
    Ogre::MeshManager::getSingleton().
            createPlane(
            "plane",
            Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
            plane,
            150, 150, 20, 20, true, 1, 5, 5,
            Ogre::Vector3::UNIT_Z
            );

    // Associate a floor entity with the created plane
    Ogre::Entity* floor = this->m_SceneMgr->createEntity("floor", "plane");
    floor->setMaterialName("Mat");
    this->m_SceneMgr->getRootSceneNode()->attachObject(floor);

    // Load model entity
    Ogre::Entity* ninja =
            this->m_SceneMgr->createEntity(
            "skeleton", "skeleton.mesh"
            );
    ninja->setCastShadows(true);
    Ogre::AxisAlignedBox bbox = ninja->getBoundingBox();

    // Associate it to a node
    Ogre::SceneNode* skeleton_node =
            this->m_SceneMgr->getRootSceneNode()->createChildSceneNode(
            "skeleton_node"
            );
    skeleton_node->attachObject(ninja);
    skeleton_node->translate(0, -bbox.getMinimum()[ 1 ], 0);
    skeleton_node->rotate(Ogre::Quaternion(Ogre::Degree(90), Ogre::Vector3(1,0,0)) , Ogre::Node::TS_WORLD);

    // Prepare skeleton to be manually controlled
    //Ogre::SkeletonInstance* ninja_skel = ninja->getSkeleton();
    //for (unsigned int bIt = 0; bIt < ninja_skel->getNumBones(); bIt++)
    //    ninja_skel->getBone(bIt)->setManuallyControlled(true);
}

// -------------------------------------------------------------------------

void BaseView::
_9_createFrameListener() {
    this->m_Controller =
            new VRPN_Controller(this->m_Window, this->m_Camera, this);
    this->m_Root->addFrameListener(this->m_Controller);
}

// -------------------------------------------------------------------------

void BaseView::
_10_createAnimations() {
    //Ogre::Entity* ninja = this->m_SceneMgr->getEntity("skeleton");
    //if (ninja == NULL)
    //    return;

    /*
      hand.l
      upper_arm.r
      hip.r
      upper_arm.l
      hand.r
      shoulder.l
      finger1.l
      finger2.l
      hip.l
      spine1
      spine3
      spine2
      spine5
      spine4
      finger2.r
      master
      finger1.r
      shoulder.r
      knee.r
      knee.l
      head
      foot.l
      kneer.r
      upper_leg.r
      lower_arm.l.003
      lower_arm.l.002
      lower_arm.l.001
      kneel.l
      lower_arm.l
      leg.l
      leg.r
      lower_arm.r
      toe.r
      upper_leg.l
      foot.r
      lower_arm.r.001
      lower_arm.r.003
      lower_arm.r.002
      neck
      lower_leg.r
      toe.l
      lower_leg.l
     
    Ogre::Bone* bone = ninja->getSkeleton()->getBone("neck");
    if (bone == NULL)
        return;

    Ogre::Real duration = 4.0;
    unsigned int numKeys = 10;
    Ogre::Real maxAngle = 30;

    Ogre::Real step = duration / Ogre::Real(numKeys);
    Ogre::Real offAngle = Ogre::Real(4) * maxAngle / Ogre::Real(numKeys);

    Ogre::Animation* animation = this->m_SceneMgr->
            createAnimation("NinjaAnim", duration);
    animation->setInterpolationMode(Ogre::Animation::IM_SPLINE);
    Ogre::NodeAnimationTrack* track = animation->createNodeTrack(0, bone);
    Ogre::TransformKeyFrame* key;
    for (unsigned int k = 0; k <= numKeys; k++) {
        key = track->createNodeKeyFrame(Ogre::Real(k) * step);

        unsigned int rk = (k > (numKeys >> 1)) ? numKeys - k : k;
        Ogre::Real angle = (offAngle * Ogre::Real(rk)) - maxAngle;
        key->setRotation(
                Ogre::Quaternion(Ogre::Degree(angle), Ogre::Vector3::UNIT_Y)
                );

    } // rof

    this->m_AnimationState =
            this->m_SceneMgr->createAnimationState("NinjaAnim");
    this->m_AnimationState->setEnabled(true);
    this->m_AnimationState->setLoop(true);
     */
}

// eof - $RCSfile$
