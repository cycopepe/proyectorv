#ifndef __BASEVIEW__H__
#define __BASEVIEW__H__

#include <Ogre.h>
#include <OgreConfigFile.h>

#include "../ogre_EXPORT.h"
#include "../controller/VRPN_Controller.h"

/**
 */
class ogre_EXPORT BaseView
{
public:
  BaseView( );
  virtual ~BaseView( );

  virtual void run( );
  virtual void updateSimulation( const Ogre::Real& timeoffset );

protected:
  virtual bool setup( );

  virtual void _0_destroyScene( );
  virtual void _1_setupResources( );
  virtual bool _2_configure( );
  virtual void _3_chooseSceneManager( );
  virtual void _4_createCamera( );
  virtual void _5_createViewports( );
  virtual void _6_createResourceListener( );
  virtual void _7_loadResources( );
  virtual void _8_createScene( );
  virtual void _9_createFrameListener( );
  virtual void _10_createAnimations( );

protected:
  Ogre::String m_ResourcePath;
  Ogre::String m_ConfigPath;

  Ogre::Root*         m_Root;
  Ogre::SceneManager* m_SceneMgr;
  Ogre::RenderWindow* m_Window;
  Ogre::Camera*       m_Camera;

  VRPN_Controller*      m_Controller;
  Ogre::AnimationState* m_AnimationState;
};

#endif // __BASEVIEW__H__

// eof - $RCSfile$
