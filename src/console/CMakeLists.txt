
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR}/src/devices)

ADD_EXECUTABLE(test_wiimote test_wiimote.cxx)
TARGET_LINK_LIBRARIES(test_wiimote
 ${Boost_FILESYSTEM_LIBRARY}
   ${Boost_SYSTEM_LIBRARY}
 Devices 
)

## eof - CMakeLists.txt
