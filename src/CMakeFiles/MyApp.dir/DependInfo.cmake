# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/cycopepe/Documents/RV/proyecto/primera entrega/src/main.cxx" "/home/cycopepe/Documents/RV/proyecto/primera entrega/src/CMakeFiles/MyApp.dir/main.cxx.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "BOOST_ALL_NO_LIB"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/cycopepe/Documents/RV/proyecto/primera entrega/src/view/CMakeFiles/View.dir/DependInfo.cmake"
  "/home/cycopepe/Documents/RV/proyecto/primera entrega/src/controller/CMakeFiles/Controller.dir/DependInfo.cmake"
  "/home/cycopepe/Documents/RV/proyecto/primera entrega/src/model/CMakeFiles/Model.dir/DependInfo.cmake"
  "/home/cycopepe/Documents/RV/proyecto/primera entrega/src/devices/CMakeFiles/Devices.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/ois"
  "/usr/local/include/OGRE"
  "NOTFOUND"
  "/usr/local/include"
  "/usr/local/share/OGRE/samples/Common/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
