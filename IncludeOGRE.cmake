## -------------------------------------------------------------------------
## Configure base paths
## -------------------------------------------------------------------------
IF(WIN32)
  SET(CMAKE_MODULE_PATH "$ENV{OGRE_HOME}/CMake/;${CMAKE_MODULE_PATH}")
  SET(OGRE_SAMPLES_INCLUDEPATH
    $ENV{OGRE_HOME}/Samples/include
    )
ENDIF(WIN32)

IF(UNIX)
  IF(EXISTS "/usr/local/lib/OGRE/cmake")
    SET(
      CMAKE_MODULE_PATH "/usr/local/lib/OGRE/cmake/;${CMAKE_MODULE_PATH}"
      )
    SET(
      OGRE_SAMPLES_INCLUDEPATH "/usr/local/share/OGRE/samples/Common/include/"
      )
  ELSEIF(EXISTS "/usr/lib/OGRE/cmake")
    SET(
      CMAKE_MODULE_PATH "/usr/lib/OGRE/cmake/;${CMAKE_MODULE_PATH}"
      )
    SET(
      OGRE_SAMPLES_INCLUDEPATH "/usr/share/OGRE/samples/Common/include/"
      )
  ELSE(EXISTS "/usr/local/lib/OGRE")
    MESSAGE(FATAL_ERROR "Failed to find module path.")
  ENDIF(EXISTS "/usr/local/lib/OGRE/cmake")
ENDIF(UNIX)

IF(CMAKE_BUILD_TYPE STREQUAL "")
  # CMake defaults to leaving CMAKE_BUILD_TYPE empty. This screws up
  # differentiation between debug and release builds.
  SET(
    CMAKE_BUILD_TYPE "Release"
    CACHE STRING "Choose the type of build, options are: None (CMAKE_CXX_FLAGS or CMAKE_C_FLAGS used) Debug Release RelWithDebInfo MinSizeRel."
    FORCE
    )
ENDIF(CMAKE_BUILD_TYPE STREQUAL "")

SET(CMAKE_DEBUG_POSTFIX "_d")
SET(CMAKE_INSTALL_PREFIX "${CMAKE_CURRENT_BINARY_DIR}/dist")

## -------------------------------------------------------------------------
## Find OGRE
## -------------------------------------------------------------------------
FIND_PACKAGE(OGRE REQUIRED)
#IF(NOT "${OGRE_VERSION_NAME}" STREQUAL "Byatis")
#  MESSAGE(FATAL_ERROR "You need Ogre 1.8 Byatis to build this.")
#ENDIF(NOT "${OGRE_VERSION_NAME}" STREQUAL "Byatis")

## -------------------------------------------------------------------------
## Find OIS
## -------------------------------------------------------------------------
FIND_PACKAGE(OIS REQUIRED)
IF(NOT OIS_FOUND)
  MESSAGE(FATAL_ERROR "Failed to find OIS.")
ENDIF(NOT OIS_FOUND)

## -------------------------------------------------------------------------
## Find Boost
## -------------------------------------------------------------------------
IF(NOT OGRE_BUILD_PLATFORM_IPHONE)
  IF(WIN32 OR APPLE)
    SET(Boost_USE_STATIC_LIBS TRUE)
  ELSE(WIN32 OR APPLE)
    SET(Boost_USE_STATIC_LIBS ${OGRE_STATIC})
  ENDIF(WIN32 OR APPLE)
  IF(MINGW)
    # this is probably a bug in CMake: the boost find module tries to look for
    # boost libraries with name libboost_*, but CMake already prefixes library
    # search names with "lib". This is the workaround.
    SET(CMAKE_FIND_LIBRARY_PREFIXES ${CMAKE_FIND_LIBRARY_PREFIXES} "")
  ENDIF(MINGW)
  SET(
    Boost_ADDITIONAL_VERSIONS
    "1.44" "1.44.0" "1.42" "1.42.0" "1.41.0" "1.41" "1.40.0" "1.40"
    "1.39.0" "1.39" "1.38.0" "1.38" "1.37.0" "1.37" "1.54" "1.54.0"
    )
  SET(OGRE_BOOST_COMPONENTS thread date_time)
  FIND_PACKAGE(Boost COMPONENTS ${OGRE_BOOST_COMPONENTS} QUIET)


  IF(NOT Boost_FOUND)
    MESSAGE(FATAL_ERROR "Failed to find BOOST.")
    SET(Boost_USE_STATIC_LIBS NOT ${Boost_USE_STATIC_LIBS})
    FIND_PACKAGE(Boost COMPONENTS ${OGRE_BOOST_COMPONENTS} QUIET)
  ENDIF(NOT Boost_FOUND)
  FIND_PACKAGE(Boost QUIET)
  
  # Set up referencing of Boost

  INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIR})
  ADD_DEFINITIONS(-DBOOST_ALL_NO_LIB)
  SET(OGRE_LIBRARIES ${OGRE_LIBRARIES} ${Boost_LIBRARIES})
 	set(Boost_USE_MULTITHREADED ON)

ENDIF(NOT OGRE_BUILD_PLATFORM_IPHONE)

## -------------------------------------------------------------------------
## Configure paths
## -------------------------------------------------------------------------
INCLUDE_DIRECTORIES( ${OIS_INCLUDE_DIRS}
  ${OGRE_INCLUDE_DIRS}
  ${OGRE_SAMPLES_INCLUDEPATH}
  )

## -------------------------------------------------------------------------
## Find OGRE plugins installed in current system
## -------------------------------------------------------------------------
IF(NOT EXISTS ${OGRE_PLUGIN_DIR_REL})
  GET_FILENAME_COMPONENT(
    OGRE_LIBRARY_PATH
    ${OGRE_LIBRARY_REL}
    PATH
    CACHE
    )
  SET(
    OGRE_PLUGIN_DIR
    "${OGRE_LIBRARY_PATH}/OGRE"
    CACHE PATH "Path to installed plugins"
    )
ELSE(NOT EXISTS ${OGRE_PLUGIN_DIR_REL})
  SET(
    OGRE_PLUGIN_DIR
    "${OGRE_PLUGIN_DIR_REL}"
    CACHE PATH "Path to installed plugins"
    )
ENDIF(NOT EXISTS ${OGRE_PLUGIN_DIR_REL})

SET(OGRE_PLUGINS
  RenderSystem_Direct3D9
  RenderSystem_Direct3D10
  RenderSystem_Direct3D11
  RenderSystem_GL
  RenderSystem_GLES
  Plugin_ParticleFX
  Plugin_BSPSceneManager
  Plugin_CgProgramManager
  Plugin_PCZSceneManager
  Plugin_OctreeZone
  Plugin_OctreeSceneManager
  )
SET(OGRE_INSTALLED_PLUGINS "")
FOREACH(loop_var ${OGRE_PLUGINS})
  IF(EXISTS ${OGRE_${loop_var}_LIBRARY_REL})
    SET(
      OGRE_INSTALLED_PLUGINS
      "${OGRE_INSTALLED_PLUGINS}\nPlugin=${loop_var}"
      )
  ENDIF(EXISTS ${OGRE_${loop_var}_LIBRARY_REL})
ENDFOREACH(loop_var)
CONFIGURE_FILE(
  ${PROJECT_SOURCE_DIR}/plugins.cfg.in
  ${PROJECT_BINARY_DIR}/plugins.cfg
  @ONLY
  )

## eof - IncludeOGRE.cmake
